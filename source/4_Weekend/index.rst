:orphan:

Weekend entre amis
---------------------


.. admonition:: Objectifs
   :class: note
   
   Consolider les notons abordées précédemment en résolvat un petit problème


.. figure:: ../images/pingouincalcul.png
   :height: 5em
   :align: right


.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


