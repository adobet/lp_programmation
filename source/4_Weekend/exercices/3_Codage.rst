
.. raw:: html

   <span class="exo"></span>
   
Codage d'une modélisation
++++++++++++++++++++++++++++++

On propose la modélisation suivante :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Personne {
   - nom: String
   + Personne(nom:String)
   }

   class Depense {
   - montant: double
   - produit : String
   + Depense(payeur:Personne, montant:double, produit:String)
   }
   
   class WeekEnd {
   - nom: String
   + WeekEnd(nom: String)
   + ajoutePersonne(personne:Personne):void
   + ajouteDepense(payeur:Personne, montant:double, produit:String):void
   + totalDepensesPersonne(personne:Personne):double
   + totalDepenses():double
   + depenseMoyenne():double
   + equilibrer(personne:Personne):double
   }

   Depense "-payeur" --> "1" Personne
   WeekEnd "-listeDesDepenses" --> "*" Depense
   WeekEnd "-listeDesPersonnes" --> "*" Personne

   note left of WeekEnd
      Attention !
      Cette classe possède trois attributs
   end note

   note right of Depense
      Attention !
      Cette classe possède trois attributs
   end note
   
   @enduml




#. Initiez un projet pour cet exercice. Coder les trois classes de façon à ce que votre code compile.

.. admonition: Remarque

   * Pour le moment, ces classes ne font rien ;
   * n'oubliez pas de versionner à la fin de chaque question ;
   * dans le diagramme de classes, il n'y a ni "getter", ni "setter". Vous pourrez en ajouter si le besoin s'en fait sentir.



#. Créez une classe ``Executable`` contenant la modélisation de la situation suivante :

Pierre a acheté du pain pour 12 euros, Paul a dépensé 100 euros pour les pizzas, Pierre a payé l’essence et en a
eu pour 70 euros, Marie a acheté du vin pour 15 euros, Paul a aussi acheté du vin et en a eu pour 10 euros, Anna
n’a quant à elle rien acheté.

#. Dans l'exécutable, ajoutez :

 * un appel à la méthode permettant de connaître les dépenses totales du week end.
 * des appels permettant de tester toutes les méthodes de vos classes.


#. Implémentez les constructeurs : ``Personne``, ``Depense`` et ``WeekEnd``. Vérifiez que votre code compile et s'exécute correctement. Versionnez.

#. Implémenter les méthodes ``ajoutePersonne()`` et ``ajouteDepense()``

#.  Implémenter les méthodes ``totalDepensesPersonne()| et \verb|totalDepenses()``
#.  Implémenter la méthode ``depensesMoyenne()``
#.  Implémenter la méthode ``equilibrer()``
#.  (Bonus) Ajouter la méthode ``depenseProduit(produit:String):double`` sans oublier d'ajouter des appels dans votre exécutable.
