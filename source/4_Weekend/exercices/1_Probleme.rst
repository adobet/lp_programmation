.. raw:: html

   <span class="exo"></span>


Le problème
+++++++++++

Plusieurs amis organisent régulièrement des week end. Ils souhaitent que vous les aidiez à développer une application qui les aiderait à faire les comptes suite aux dépenses de chacun.

.. figure:: ../../images/pingouinPainVin.jpg
   :height: 5em
   :align: left


#. Par exemple, trois d'entre eux se sont retrouvés pour le week end du 15 août. Lors de ce week end, Anna a dépensé 20 euros pour le cinéma et 30 euros pour la nourriture. Barnabé  a dépensé 10 euros pour les boissons. Chris a payé l'essence et en a eu pour 15 euros.


   a) Quelle somme totale a été dépensée pendant ce week end ?
   #) Calculer la somme dépensée par chacun des participants
   #) Calculer la somme que chaque personne doit verser/recevoir pour équilibrer les dépenses.

   

#. Les amis ont organisé un nouveau week end pour l'anniversaire de Pierre. Lors de ce week end, Pierre a acheté du pain pour 12 euros, Paul a dépensé 100 euros pour les pizzas, Pierre a payé l’essence et en a eu pour 70 euros, Marie a acheté du vin pour 15 euros, Paul a aussi acheté du vin et en a eu pour 10 euros, Anna n’a quant à elle rien acheté.


   a) Quelle somme totale a été dépensée pendant ce week end ?
   #) Calculer la somme dépensée par chacun des participants
   #) Calculer la somme que chaque personne doit verser/recevoir pour équilibrer les dépenses.


