
.. raw:: html

   <span class="exo"></span>
   
Analyse du problème : élaboration d'un diagramme de classes
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

On vous propose de créer les classes : ``Personne``, ``Depense`` et ``WeekEnd``.

#. Quels attributs doivent avoir les différentes classes pour pouvoir représenter les données ?

#.  Quelles méthodes et avec quels profils devez vous ajouter pour pouvoir :

   a) créer une personne, créer une dépense, créer un week end ;
   #) ajouter une personne ou une dépense à un week end ;
   #)  savoir combien a dépensé une personne ;
   #)  savoir combien d’argent a été dépensé pour un produit donné.
   #)  déterminer la somme que chaque personne doit verser/recevoir pour équilibrer les dépenses ?


Elaborez un diagramme de classe pour cette application


.. raw:: latex

   \newpage
