public class Depense
 {
    /** la personne qui a réglé la dépense **/
    private Personne personne;
    /** le montant de la dépense en euro **/
    private double montant; 
    /** le nom du motif de la dépense **/
    private String produit;
    
    public Depense(Personne personne, double montant, String produit )
    {
        this.personne = personne; 
        this.montant = montant;
        this.produit = produit;
    }
    
    public Personne getPersonne()
    {
        return this.personne;
    }
    
    public String getProduit()
    {
        return this.produit;
    }
    
    public double getMontant()
    {
        return this.montant;
    }
}

