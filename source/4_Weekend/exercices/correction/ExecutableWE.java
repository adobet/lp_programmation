
public class ExecutableWE {
    public static void main(String [] args) {
        
        //=========================================================
        // Tests pour la classe Personne
        //=========================================================

        Personne anna = new Personne("Anna"); 
        assert "Anna".equals(anna.getNom());

        Personne pierre = new Personne("Pierre"); 
        assert "Pierre".equals(pierre.getNom());


        Personne toto = new Personne("Anna"); 

        assert !anna.equals(pierre);
        assert anna.equals(toto);
        
        
        Personne paul = new Personne("Paul");
        Personne marie = new Personne("Marie");

        //=========================================================
        // Tests pour la classe Dépense   
        //=========================================================
        Depense d1 = new Depense(pierre, 12, "pain"); 
        assert pierre.equals(d1.getPersonne());
        assert Math.abs(d1.getMontant()-12) <0.001;
        assert "pain".equals(d1.getProduit());
        

        Depense d2 = new Depense(paul, 100, "pizza"); 
        Depense d3 = new Depense(pierre, 70, "essence");
        Depense d4 = new Depense(marie, 15, "vin");  
        Depense d5 = new Depense(paul, 10, "vin");
        
        //=========================================================
        // Quelques Tests pour la classe WeekEnd   
        //=========================================================
        WeekEnd we = new WeekEnd("anniv Pierre");
        we.ajoutePersonne(pierre); 
        we.ajoutePersonne(paul);
        we.ajoutePersonne(marie);
        we.ajoutePersonne(anna); 
        we.ajouteDepense(pierre, 12, "pain");   
        we.ajouteDepense(paul, 100, "pizza"); 
        we.ajouteDepense(pierre, 70, "essence");   
        we.ajouteDepense(marie, 15, "vin");
        we.ajouteDepense(paul, 10, "vin");
        
        // Quelques vérifications
        System.out.println("total des dépenses "+we.totalDepenses()); //207.0
        assert Math.abs(we.totalDepenses()-207.0)<0.001;
        
        System.out.println("moyenne des dépenses "+we.depensesMoyenne()) ; //51.75
        assert Math.abs(we.depensesMoyenne()-51.75)<0.001;
        
        System.out.println("total des dépenses de Pierre "+ we.totalDepensesPersonne(pierre)) ; //82.0
        assert Math.abs(we.totalDepensesPersonne(pierre)-82.0)<0.001;
        
        System.out.println("total des dépenses de Paul "+ we.totalDepensesPersonne(paul)) ; //110.0
        assert Math.abs(we.totalDepensesPersonne(paul)-110.0)<0.001;
        
        System.out.println("total des dépenses de Marie "+ we.totalDepensesPersonne(marie)) ; //15.0
        assert Math.abs(we.totalDepensesPersonne(marie)-15.0)<0.001;
        
        System.out.println("total des dépenses de Anna "+ we.totalDepensesPersonne(anna)) ; //0.0
        assert Math.abs(we.totalDepensesPersonne(anna))<0.001;
        
        System.out.println("avoir de pierre "+we.equilibrer(pierre)); // -30.25
        assert Math.abs(we.equilibrer(pierre)-(-30.25))<0.001;
        
        System.out.println("avoir de paul "+we.equilibrer(paul)); //-58.25
        assert Math.abs(we.equilibrer(paul)+58.25)<0.001;
        
        System.out.println("avoir de marie "+we.equilibrer(marie)); //36.75
        assert Math.abs(we.equilibrer(marie)-36.75)<0.001;
        
        // Pour aller plus loin
         System.out.println("total des dépenses de Pierre "+ we.totalDepensesPersonne(new Personne("Pierre"))) ; //82.0    
        assert Math.abs(we.totalDepensesPersonne(new Personne("Pierre")) -82.0)<0.001;   
    }
}
