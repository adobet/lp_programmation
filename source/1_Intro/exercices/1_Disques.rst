.. figure:: ../../images/disquesColores.png
   :height: 5em
   :align: right
   


.. raw:: html

   <span class="exo"></span>

Disques colorés
+++++++++++++++


**Objectif** : *Concevoir une classe*


Je veux concevoir une application qui dessine des disques de couleur que je peux agrandir si je le souhaite. Pour créer ces disques, j'ai besoin d'une **classe**.


#. Proposer un nom pour cette **classe**.

#. Proposer des attributs pour cette **classe**.

#. Proposez des méthodes pour cette **classe**.

#. Proposez deux instances de cette classe.


.. admonition:: Bonnes pratiques
   :class: hint

   le nom des classes commencent par une majuscule. 
   le nom des attributs et des méthodes commencent par une minuscule
