.. raw:: latex

   \newpage
   

.. raw:: html

   <span class="exo"></span>

la classe Timer
++++++++++++++++++++++++++++++

**Objectif** : *Javadoc et représentation UML*


On donne ci-dessous la javadoc d'une classe `Timer`.



.. raw:: html

   <a class="reference external" target="_blank" href="../../_static/docTimer/Timer.html">lien vers la javadoc de la classe Timer</a>
   
   <p><p>Screenshots :
   


.. image:: ./src/TimerDoc2.pdf
   :height: 0.1em
   :align: right
   :alt: javadoc de la classe Timer (page2)
   
.. image:: ./src/TimerDoc1.pdf
   :height: 0.1em
   :align: right
   :alt: javadoc de la classe Timer (page1)

   
.. raw:: laTex
   
   \vspace{-6em}
   \includegraphics[width=0.9\textwidth]{TimerDoc1.pdf}
   \includegraphics[width=0.9\textwidth]{TimerDoc2.pdf}
   \vspace{-3em}

   

#. Combien y a-t-il de constructeurs ? 

#. Combien y a-t-il de méthodes ? 

#. Combien y a-t-il d'attributs ?

#. Donner une représentation UML de la classe `Timer`.

   



