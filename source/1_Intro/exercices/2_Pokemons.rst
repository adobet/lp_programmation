
.. figure:: ../../images/bulbizarre.png
   :height: 3em
   :align: right

.. raw:: html

   <span class="exo"></span>

La classe Pokemon
+++++++++++++++++++

**Objectif** : *Lire un diagramme de classe simple*


Voici un exemple de diagramme UML de la classe ``Pokemon`` :


.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Pokemon {
   
   - nom: String
   - pv: int = 50
   - attaque: double
   - types[1-3]: String
   + Pokemon(nom: String, attaque:int, type:String)
   + Pokemon(nom: String, attaque:int, type1:String, type2:String)
   + Pokemon(nom: String, attaque:int, type1:String, type2:String, type3:String)
   + getNom(): String
   + getPV(): int
   + getTypes(): String[*]
   + attaque(autrePokemon: Pokemon): void
   + estVaincu(): boolean
   + seSoigne(soin: int): void
   + evolue(nouveauNom: String, nouvelleAttaque: int): void
   }
      
   @enduml


#. Combien y a-t-il de constructeurs ? Combien y a-t-il de méthodes ? Combien y a-t-il d'attributs ?

#. A quoi correspondent les symboles + et - ?

#. Il manque un accesseur. Lequel ?

#. Donner deux exemples d'instance de cette classe.

