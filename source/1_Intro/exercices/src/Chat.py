class Chat:
    """ 
        Classe Chat en Python définie par :
            - son nom
    """
    
    def __init__(self, nom):
        """ Constructeur de Chat """
        self._nom=nom

    def get_nom():
        return self._nom

    def set_nom(self, nouveauNom):
        self._nom=nouveauNom            
    
    nom=property(get_nom, set_nom)
    
    def miaule(self):
        """
            Cette méthode fait miauler le chat en affichant Miaou !!!
            sur la sortie standard (dans le terminal par défaut)
        """
        print("Miaou !!!")
    
    def estEndormi(self, heure):
        """
            Cette méthode détermine si le chat est endormi
        
            :param heure: c'est un nombre compris entre 0 et 24 
                qui précise l'heure de la journée
            :type heure: double
            :return: True si le chat dort (presque tout le temps) et False sinon (c'est à dire entre 3 et 4 heures du matin)
        """    
        return heure<=3 or heure>4;
