/** Classe Chat en JAVA */

public class Chat{

	/** Nom du chat */
	private String nom;
	
	/** Constructeur de Chat */
	public Chat(String nomDuChat){
		this.nom=nomDuChat;
	}
	
	// Accesseurs ===================
	public String getNom(){
		return this.nom;
	}
	
	// Mutateurs ===================
	public void setNom(String nouveauNom){
		return this.nom=nouveauNom;
	}
	
	// Autres méthodes ===================
	
	/**
	 * Cette méthode fait miauler le chat en affichant un message
	 * sur la sortie standard (dans le terminal par défaut)
	 */
	public void miaule(){
		System.out.println("Miaou !!!!");
	}
	
	/**
	 * Cette méthode détermine si le chat est endormi
	 
	 * @param heure un double compris entre 0 et 24 qui 
	 * précise l'heure de la journée
	 * @return true si le chat dort (presque tout le temps) et false sinon
	 * (c'est à dire entre 3 et 4 heures du matin)
	 */
	public boolean estEndormi(double heure){
		return (heure<=3 || heure>4);
	}
}
