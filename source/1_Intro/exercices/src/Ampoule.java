public class Ampoule {
    /** la couleur est modélisé par un nombre entier */
    private int couleur;
    private int puissance;
    private boolean eteinte;
    
    /** Construit une lampe éteinte */
    public Ampoule(int couleur, int puissance) {
        this.couleur = couleur;
        this.puissance = puissance;
        this.eteinte=true;
    }
    /** Construit une lampe éteinte, de couleur 0 et de puissance nulle */
    public Ampoule() {
        this(0,0);
    }
    
    public int getCouleur() {
        return this.couleur;
    }
    public int getPuissance() {
        return this.puissance;
    }
    public void setCouleur(int couleur) {
        this.couleur = couleur;
    }
    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }
    public void allumer() {
        this.eteinte=false;
    }
    public void eteindre() {
        this.eteinte=true; 
    }
    public String toString() {
        String etat;
        if (this.eteinte) etat="éteinte"; 
        else etat="allumée";
        return "Ampoule [couleur=" + this.couleur + ", puissance=" + 
            this.puissance + "]"+ ", etat=" + etat + "]";
    }
}
