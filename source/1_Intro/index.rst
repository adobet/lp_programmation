
Notions de `Classe` et `Objets`
====================================


.. admonition:: Objectifs
   :class: note

   * Comprendre/utiliser le vocabulaire de base de la Programmation Orientée Objet (POO)
   * Lire/écrire un diagramme de classe simple (diagrammesUML)
   * Connaître et appliquer quelques bonnes pratiques de programmation/conception
  
   
.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*
