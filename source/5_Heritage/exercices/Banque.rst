.. raw:: html

   <span class="exo"></span>


Banques d'un consortium
++++++++++++++++++++++++++++

**Objectif** : *Factorisation en utilisant l'héritage*



Une première analyse a conduit à établir les classes ci-dessous pour l'organisation des banques d'un consortium. Ce n’est qu'un premier jet qu'il faut s'efforcer de corriger : Certains attributs sont redondants

.. figure:: ../../images/argent.jpeg
   :height: 5em
   :align: right


.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Employe {
   - nom : String
   - prenom : String
   - adresse : String
   - salaire : float
   - numINSEE : int
   - nomBanque : String
   + numerosClientsConseils() : String[*]
   }

   class Agence {
   - numero : String
   - nom : String
   - adresse : String
   - ville : String
   - codePostal : String
   - nomDirecteur : String
   - nomBanque : String}
   + numerosComptesAvecInterets() : String[*]
   + numeroComptesSansInterets() : String[*]
   + listeComptes() : ?
   + listeEmploye() : Employes[*]
   }
   
   class Banque {
   - nom : String
   - capital : float
   - adresseSiege : String
   - numSalaries : int[*]
   + villesAgences() : String[*]
   }

   class CompteAvecInterets {
   - numero : String
   - solde : float
   - dateOuverture : String
   - taux : float
   - nomClient : String
   - prenomClient : String
   - numeroAgence : String
   - numeroBanque : String
   + getClient() : Client
   }
   
   class CompteBanquaire {
   - numero : String
   - solde : float
   - dateOuverture : String
   - nomClient : String
   - prenomClient : String
   - numeroAgence : String
   - numeroBanque : String
   + getClient() : Client
   }
   
   class Client {
   - numero : String
   - nom : String
   - prenom : String
   - adresse : String
   - nomConseiller : String
   - nomBanque : String}
   + numerosComptesAvecInterets() : String[*]
   + numerosComptesSansInterets() : String[*]
   + listeComptes() : ?
   }
      
   @enduml



#. Introduisez des associations entre classes pour limiter la redondance d’informations. On supposera que les associations introduites donnent lieu à des accesseurs (getters, setters) sans qu’il soit nécessaire de les reprendre tous sur le diagramme.

#. Utilisez l’héritage pour optimiser le modèle. Vous pouvez introduire une nouvelle classe si nécessaire.

