import java.util.List;
import java.util.ArrayList;

class Droide extends Article
{
    private String fonction;
        
    public Droide(String nom, double etat, String fonction, String image)
    {
    }
}

class Vaisseau extends Article
{
    private String classe;
    
    public Vaisseau(String nom, double etat, String classe, String image)
    {
    }
}

abstract class Article implements Vendable
{
    protected String image;
    
    public String getType(){
        return null;
    }
    public String getNom(){
        return null;
    }
    public int getEtat(){
        return 0;
    }
    public String getInfo(){
        return null;
    }
    public void reparer(){}
    
    public String getImage(){
        return null;
    }
    
}

interface Vendable{

    public String getType();
    public String getNom();
    /** l'état d'un article est un nombre entre 0 et 100
     * 0% = à mettre à la poubelle
     * 100% = état neuf
     */
    public int getEtat(); 
    public String getInfo();
    /** Cette méthode change augmente l'état de l'article de 10 points
     * sans dépasser 100% 
     */
    public void reparer();
}

class Stock{

    private String proprietaire;
    private List<Vendable> leStock;
    private Article articleEnPresentation;
    
    public Stock(String nom)
    {
        this.proprietaire = nom;
        this.leStock = new ArrayList<>();
    }
    
    public void vendre(Article article, String acheteur){}
    
    public void ajouter(Article article){}
    
    public void suivant(){}
    
    public void precedent(){}
   
    
}
