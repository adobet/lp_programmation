Exercice Generique
-------------------

Dans cet exercice, écrivez un nombre strictement supérieur à 3 !


.. easypython:: /99_A_CLASSER/exercices/exo_generique.py
   :language: generique
   :uuid: 1231313
