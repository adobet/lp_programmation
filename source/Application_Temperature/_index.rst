:orphan:

Mini projet : Application Températures
----------------------------------------

.. admonition:: Objectifs
   :class: note

   * Développer une mini application en mettant en application le modèle MVC


L'objectif de ce mini-projet guidé est de créer une application permettant le contrôle de la température en degrés Celsius ou Farenheit. 
L'application se compose de trois boîtes représentant la même température sous des formes différentes. La modification d’une des boîtes doit mettre automatiquement à jour les autres. 

.. figure:: ../images/temperatures.png
   :width: 100%
   :align: center

.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


