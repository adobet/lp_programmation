.. raw:: html

   <span class="exo"></span>
   

Les Contrôleurs
++++++++++++++++++++++++++++

Le contrôleur est composé des classes `GestionCelsius`, `GestionFarenheit` et  `GestionTemperatures`.


#. Il faut connecter les contrôleurs avec les widgets de la vue. Dans la classe `AppliTemperature`, décommentez   les lignes de code qui correspondent à cet exercice puis complétez-les de façon à ce que le code compile.

   Vérifiez ensuite que l'application s'exécute (presque) correctement.


#. Il reste un widget non connecté à un contrôleur. Lequel ? Connectez-le avec son contrôleur.

   Compilez et vérifiez que l'application s'exécute correctement.


#. Les classes `GestionCelsius`, `GestionFarenheit` contiennent du code redondant. Quelle solution proposez-vous ?


