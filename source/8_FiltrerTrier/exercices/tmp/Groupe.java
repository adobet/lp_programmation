import java.util.List;
import java.util.ArrayList;

public class Groupe{
    
    private String nom;
    private List<Personnage> membresDuGroupe;
    
    public Groupe(String nom){
        this.nom = nom;
        this.membresDuGroupe = new ArrayList<>(); // instancie une nouvelle liste vide
    }
    
    public void ajoute(Personnage perso){
        this.membresDuGroupe.add(perso);
    }
    
    public void ajoute(String nom, int barbe, int oreilles){
        this.ajoute(new Personnage(nom, barbe, oreilles));
    }
    
    @Override
    public String toString(){
        return this.nom+"\n"+this.membresDuGroupe.toString();
    }
   
   public List<Personnage> filtreOreille(int tailleMini){
   
        List<Personnage> grandesOreilles = new ArrayList<>();
        // For each
        for(Personnage perso : this.membresDuGroupe){
            if (perso.getOreilles() > tailleMini){
                grandesOreilles.add(perso);
            }
            // else rien
        }
        
        return grandesOreilles;
   }

   public List<Personnage> filtreBarbe(int tailleMaxi){
        List<Personnage> petiteBarbe = new ArrayList<>();
        // For par indice
        for(int i = 0; i < this.membresDuGroupe.size(); i++){
            if (this.membresDuGroupe.get(i).getBarbe() <= tailleMaxi ){
                petiteBarbe.add(this.membresDuGroupe.get(i));
            }
            // else toujours rien
        }
        return petiteBarbe;       
   }
}
