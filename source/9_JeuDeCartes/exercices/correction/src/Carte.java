
/**
 * Cette classe modélise les cartes à jouer d'un jeu de 52 cartes
 * les valeurs étant, dans l'ordre, "As", "2", "3", ... "9", "10", "Valet", "Dame" et "Roi"
 * les couleurs étant, dans l'ordre, "trefle", "carreau", "coeur", "pique".
 */

public class Carte  implements Jouable
{

    /**
     * "coeur", "carreau", "pique", "trefle"
     */
    private String couleur;
    /**
     * 1, 2, 3, ..., 9, 10, 11 (-> Valet), 12 (-> Dame), 13 (-> Roi)
     */
    private int valeur;

    public Carte(int valeur, String couleur){
        this.couleur=couleur;
        this.valeur=valeur;
    }
    
    /**
     * @return "As", "2", ..., "Dame" ou "Roi"
     */
    public String getValeur(){ 
        String res;
        switch (valeur) { // TYPES PRIMITIFS SEULEMENT
                case 1: res ="As"; break;
                case 11: res ="Valet"; break;
                case 12: res ="Dame"; break;
                case 13: res ="Roi"; break;
                default: res = ""+valeur;
        }
        return res;
    } 

    /**
     * @return 1, 2, 3 .., , "2", ..., "Dame" ou "Roi"
     */
    public int getValeurInt(){ 
        return this.valeur;
    } 
        
    public String getCouleur(){ return couleur;}
    
    @Override
    public String toString(){
        return getValeur()+" de "+couleur;
    }

    @Override
    public boolean equals(Object other){ // Attention "Object" obligatoire
        boolean result;
        if((other == null) || (this.getClass()!= other.getClass())){
            result = false;
        } // end if
        else{ // other!=null ET this.getClass()== other.getClass()
                
            Carte otherCarte = (Carte)other; // Je caste l'Objet other en Carte
            
            result = this.couleur.equals(otherCarte.couleur) &&  this.valeur==otherCarte.valeur;
        } // end else

        return result;
    }
    
        @Override
        public int compareTo(Jouable c2) {
                return this.valeur-c2.getValeurInt();
        }
}
    
