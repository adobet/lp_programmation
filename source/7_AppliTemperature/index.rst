:orphan:

Application Températures
--------------------------

**Présentation**

   

L'objectif de cette séquence est de créer une application permettant le contrôle de la température en degrés Celsius ou Farenheit.


.. figure:: ../images/temperatures.png
   :width: 100%
   :align: center
   

L'application se compose de trois boîtes représentant la même température sous des formes différentes. La modification d’une des boîtes doit mettre automatiquement à jour les autres. 



.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


