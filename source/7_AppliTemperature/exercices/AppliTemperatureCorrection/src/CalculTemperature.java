public class CalculTemperature  {
    
    private double celsius_min;
    private double celsius_max;
    private double  celsius;
    
    public CalculTemperature(){
        this.celsius = 20;
        this.celsius_min = -20;
        this.celsius_max = 80;
    
    }
    public  double  getCelsiusMin (){
        return  this.celsius_min;
    }
    public  double  getCelsiusMax (){
        return  this.celsius_max;
    }
    public  double  getCelsius (){
        return  this.celsius;
    }
    /** Modifie la température
     * @param : nouvelleTemperature en defré Celsius
     * @exception : Lève l'exception TemperatureNonValide si la nouvelleTemperature
     * n'est pas acceptable (comprise entre  celsius_min et celsius_max)
     */
    public  void  setCelsius(double nouvelleTemperature) throws TemperatureNonValide
    {   
        if (nouvelleTemperature>=this.celsius_min && nouvelleTemperature<= this.celsius_max){
            this.celsius=nouvelleTemperature;
        }
        else{
            throw new TemperatureNonValide();
            
        }
    }
    public  double  getFarenheit (){
     return  this.celsius *9.0/5.0 + 32.0;
    }
     /** Modifie la température
     * @param : farenheit en degré Farenheit
     * @exception : Lève l'exception TemperatureNonValide si farenheit
     * n'est pas acceptable , c'est à dire si la température en Celsius
     * n'est pas comprise entre  celsius_min et celsius_max)
     */   
    public  void  setFarenheit(double  farenheit) throws TemperatureNonValide
    {
        double nouvelleTemperature = (farenheit  - 32) * 5.0 / 9.0;
        this.setCelsius(nouvelleTemperature);
    }
}

