.. raw:: html

   <span class="exo"></span>


Le Modèle
++++++++++++++++++++++++++++



Le modèle est assez simple ici. Il est constitué des classes **CalculTemperature**  et **TemperatureNonValide**

#. Récupérez le fichier **CalculTemperature.java** et complétez le code. Vérifiez que votre code compile.


#. Il faut maintenant ajouter le modèle à la vue. Compléter le code de la classe **AppliTemperature** aux endroits indiqués.

   Compilez et vérifiez que l'application s'exécute (presque) correctement.

#. Il reste à terminer la connexion : la vue doit mettre à jour son affichage en fonction des données du modèle. Compléter le code de la méthode **majAffichage**.

   .. admonition:: Remarque
      
      Pour convertir un ``double`` en ``String``, plusieurs méthodes sont possibles :
      
      * utiliser la méthode ``static toString(double)`` de la classe ``Double``
      
        .. code:: java
        
            double nombre = 3.6;
            String nombreEnString = Double.toString(nombre);
         

      * utiliser la méthode ``static valueOf(double)`` de la classe ``String``
      
        .. code:: java
           
           double nombre = 3.6;
           String nombreEnString = String.valueOf(nombre);

      
      * utiliser la méthode rapide (mais inélégante) suivante :
      
        .. code:: java
           
           double nombre = 3.6;
           String nombreEnString =nombre + "";




