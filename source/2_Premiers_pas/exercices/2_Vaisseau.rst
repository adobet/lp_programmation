.. raw:: html

   <span class="exo"></span>


La classe Vaisseau
---------------------
  
On donne le diagramme de classe  d'une classe ``Vaisseau`` :


.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Vaisseau {
   
   - nom: String
   - nombreDePassagers: int=0
   - puissanceDeFeu: int
   + Vaisseau(nom: String, puissance:int)
   + Vaisseau(nom: String, passagers:int, puissance:int)
   + getNom(): String
   + nbPassagers(): int
   + getPuissance(): int
   + transportDePassagers(): boolean
   }
      
   @enduml


Voici une classe Executable :

.. literalinclude:: ./src/ExecutableVaisseau.java
   :language: java


#. Décrire le vaisseau créé ici.

#. Que fait cet exécutable ?


#. Écrire le code de la classe ``Vaisseau``

Quelques précisions :

* La variable `nombreDePassagers` indique le nombre maximum de passagers que le vaisseau peut transporter.

* La méthode `transportDePassagers` renvoie un booléen indiquant si le vaisseau est capable de transporter des passagers.


#. Vérifier que votre fichier compile.


#. Vérifier que le fichier ``ExecutableVaisseau.java`` compile.


#. Vérifier que le code de votre classe ``Vaisseau`` est correcte : 


.. easypython:: /2_Premiers_pas/exercices/TestVaisseau.java
   :language: java
   :titre: la classe Vaisseau
   :nomClasse: Vaisseau
   :nom_classe_test: TestVaisseau
