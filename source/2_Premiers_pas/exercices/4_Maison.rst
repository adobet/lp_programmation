.. raw:: html

   <span class="exo"></span>


La classe Maison
---------------------
  

On veut définir une classe `Jeu` qui doit fonctionner avec l’exécutable suivant :


.. literalinclude:: ./src/ExecutableMaison.java
   :language: java



#. Pseudo-analyse

   Identifiez le(s) constructeur(s) et les méthodes nécessaires à la classe `Maison`. Définissez les attributs qui vous semblent "raisonnables" pour définir cette classe. Résumez le tout dans un diagramme de classe.

#. Initier un projet git et y créer le fichier `ExecutableMaison.java` avec le code donné.

#. Ecrire le **code minimal** de la classe `Jeu` pour que le fichier `ExecutableMaison.java` **compile**.

#. Compléter le code de façon à pouvoir construire une `Maison` à partir de 2 paramètres. Ecrivez les getteurs et setteurs qui vous semblent nécessaires.    Vérifiez que vos fichiers compilent. Vérifiez l'exécution puis versionnez.

#. Écrivez le code des méthodes une à une. A chaque étape, compilez, vérifiez que les "pseudo-tests" qui sont dans l'exécutable passent puis versionnez. Vous pouvez ajouter des lignes de code dans l'exécutable.
   N'oubliez pas de documenter votre code si nécessaire !

#. Générez la javadoc

