class ExecutableJeu {
  public static void main(String[] args){
    Jeu j1= new Jeu("Monopoly",31,false);
    System.out.println(j1.toString()); // doit afficher :
    // Nom du jeu : Monopoly
    // Prix neuf : 31.0 euros
    // En solde à moitié prix : false
    System.out.println(j1.prix());// doit afficher 31.0
    System.out.println(j1.getPrixNeuf());// doit afficher 31.0
    j1.setSolde(true);
    System.out.println(j1.toString()); // doit afficher :
    // Nom du jeu : Monopoly
    // Prix neuf : 31.0 euros
    // En solde à moitié prix : true
    System.out.println(j1.prix());// doit afficher 15.5
  }
}
