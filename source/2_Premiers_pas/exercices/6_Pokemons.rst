
.. figure:: ../../images/bulbizarre.png
   :height: 3em
   :align: right

.. raw:: html

   <span class="exo"></span>

La classe Pokemon (suite)
++++++++++++++++++++++++++

**Objectif** : *Ecrire du code java simple*

On donne le diagramme de classe suivant :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   
   class Pokemon {
   - nom: String
   - pv: int = 50
   - attaque: int
   - type: String = "eau"
   + Pokemon(nom: String, attaque:int)
   + Pokemon(nom: String, attaque:int, type:String)
   + getNom(): String
   + getPV(): int
   + getAttaque(): double
   + getType(): String
   }
      
   @enduml
   
#. Ecrire une classe ``ExecutablePokemon`` dans laquelle :

   * on crée un pokemon appelé `Bulbizarre` qui est de type `plante` et dont la puissance d'attaque est de 3.
   
   * on crée un pokemon appelé `Tortank` qui est de type `eau` et dont la puissance d'attaque est de 4.

#. Compléter la classe ``ExecutablePokemon`` de façon à appeler chacune des méthodes de la classe `Pokemon`.

#. Ecrire la classe classe Pokemon de façon à ce que les deux fichiers compilent correctement. Vérifier que votre exécutable s'exécute correctement.


#. Vérifier que votre classe ``Pokemon`` est correcte :

.. easypython:: /2_Premiers_pas/exercices/TestPokemon.java
   :language: java
   :titre: Pokemon
   :nomClasse: Pokemon
   :nom_classe_test: TestPokemon
   

.. figure:: ../../images/defi.jpeg
   :height: 3em
   :align: left
   
On complète le diagramme de la classe `Pokemon` de la façon suivante :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Pokemon {
   
   - nom: String
   - pv: int = 50
   - attaque: int
   - types: String = "eau"
   + Pokemon(nom: String, attaque:int)
   + Pokemon(nom: String, attaque:int, type:String)
   + getNom(): String
   + getPV(): int
   + getAttaque(): double
   + getType(): String
   + evolue(nouveauNom: String, nouvelleAttaque: int): void
   + estVaincu(): boolean
   + seSoigne(soin: int): void
   + seSoigne(): void
   + attaque(autrePokemon: Pokemon): void
   }
      
   @enduml


   Quelques précisions :

   * la méthode ``evolue(nouveauNom, nouvelleAttaque)`` permet de modifier le nom et la valeur d'attaque du Pokemon.

   * la méthode ``estVaincu()`` renvoie ``true`` si les points de vie (``pv``) du Pokemon sont inférieurs à zéro, et ``false`` sinon.

   * la méthode ``seSoigne()`` prend en paramètre un entier ``soin`` qui représente le nombre de pv que regagne le Pokemon (dans la limite de 50).

   * la méthode ``seSoigne()`` sans paramètre permet au Pokemon de regagner 1 point de vie (dans la limite de 50).

   * Pour la méthode ``attaque(autrePokemon: Pokemon)`` : le pokemon le plus faible (celui qui a la valeur d'attaque la plus petite) perd 5 points de vie alors que le pokemen le plus fort n'en perd qu'un seul.

#. Pour chaque nouvelle méthode :


   Ajouter au moins un appel à cette méthode dans votre exécutable ;
   Ajouter le code **minimal** dans votre classe `Pokemon` pour que l'ensemble **compile** correctement
   Compléter le code de votre classe `Pokemon` pour que votre code **s'exécute** correctement
   Passer à la méthode suivant


.. admonition:: Bonnes pratiques
   :class: hint

   Avoir une bonne méthodologie est important !

