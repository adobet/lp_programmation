.. raw:: html

   <span class="exo"></span>


La classe Personnage
---------------------
  
On donne le diagramme de classe  d'une classe ``Personnage`` :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Personnage {
   
   - nom: String
   - tailleBarbe: int
   - tailleOreilles: int
   + Personnage(nom: String, barbe:int, oreilles:int)
   + getNom(): String
   + getBarbe(): int
   + tailleOreilles(): int
   }
      
   @enduml


Voici une classe Executable :

.. literalinclude:: ./src/ExecutablePersonnage.java
   :language: java

#. Quel est le nom du personnage créé ici ? Quelle est la taille de sa barbe ?

#. Que fait cet exécutable ?

#. Écrire dans un fichier ``Personnage.java`` le code de la classe ``Personnage``

.. admonition:: Bonnes pratiques
   :class: hint

   * une classe = un fichier (en général)
   * le nom de la classe = le nom du fichier (en général)

#. Vérifier que votre fichier ``Personnage.java`` compile.


#. Vérifier que le fichier ``ExecutablePersonnage.java`` compile.

#. Vérifier que le fichier ``ExecutablePersonnage.java`` s'exécute comme vous le souhaitez.


#. Vérifier que le code de votre classe ``Personnage`` est correcte : 


.. easypython:: /2_Premiers_pas/exercices/TestPersonnage.java
   :language: java
   :titre: la classe Personnnage
   :nomClasse: Personnage
   :nom_classe_test: TestPersonnage
