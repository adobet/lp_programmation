.. raw:: html

   <span class="exo"></span>
   

La fraternité de l'anneau
++++++++++++++++++++++++++++

.. figure:: ../../images/anneauUnique.png
   :height: 5em
   :align: right
   
On donne la classe executable suivante :

.. literalinclude:: ./src/Executable.java
   :language: java

#. Identifiez les classes nécessaires ainsi que leurs attributs et leurs méthodes. Elaborer un diagramme de classes.

#.  Ecrire le code des classes précédentes.
