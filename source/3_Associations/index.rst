:orphan:

Associations entre classes et traductions en JAVA
====================================================


.. admonition:: Objectifs
   :class: note

   * Savoir lire/écrire un diagramme de classes
   * Savoir concevoir un diagramme de classes
   * Savoir écrire/compiler/exécuter du code JAVA avec méthode
   
.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*
