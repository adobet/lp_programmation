Exemple pour des énoncés utilisant le module de test de code pour sphinx
------------------------------------------------------------------------

Installation - Procédure à partir de rien
++++++++++++++++++++++++++++++++++++++++++++

* Installer virtualenv

* Cloner le dépot dans un répertoire dédié :

  ``git clone https://gitlab.com/jrobert/easySphinx.git .``


* Créer un virtualenv et l'activer :

  ``virtualenv -p python3 venv``
  
  ``source venv/bin/activate``


* Installer les dépendances :

  ``(venv)$ pip install -r requirements.txt``


* Compiler :
    
  ``make html``


* Créer un dépot **PUBLIC** sur GitLab, puis (toujours dans le répertoire dédié) :


  ``git remote rename origin old-origin``
  
  ``git remote add origin https://gitlab.com/cprevost/lp_programmation.git``

  ``git push -u origin --all``

  ``git push -u origin --tags``
